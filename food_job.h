#ifndef HW_06_FOOD_JOB_H
#define HW_06_FOOD_JOB_H

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "LCD.h"
#include "snake_movement.h"
#include "output_styles.h"
#include "mutex_get_set.h"

#define FOOD_SIZE 20
/*
 * function takes 2 arguments and draws the square thar represents the food
 * 1. represents the x coord of the top left square of the food
 * 2. represents the y coord of the top left square of the food
 */
void draw_food(int x, int y);

/*
 * function takes 4 arguments
 * 1. represents the x coord of the snake head
 * 2. represents the y coord of the snake head
 * 3. represents the x coord of the top left square of the food
 * 4. represents the y coord of the top left square of the food
 * returns true if snakes head is in range from food else returns false
 */
bool in_food_range(int x, int y, int x_food, int y_food);

/*
 *  function takes 4 arguments and controls if snake has got the food
 * 1. represents the x coord of the snake head
 * 2. represents the y coord of the snake head
 * 3. represents the x coord of the top left square of the food
 * 4. represents the y coord of the top left square of the food
 */
void check_food(int x_snake, int y_snake, int x_food, int y_food);
/*
 * function takes 2 arguments that represents the coords of the food and
 * checks if this food will not appears on the snake
 * 1. x_food is x coord of the food
 * 2. y_food is y coord of the food
 * returns false if food wsa generated on the snake else true
 */
bool check_uniq(int x_food, int y_food);

/*
 * function takes one argument and generates the random value from 0 to the value of received argument
 */
int generate_random(int upper);
/*
 * function sets the food on the screen
 */
void set_food();

#endif //HW_06_FOOD_JOB_H
