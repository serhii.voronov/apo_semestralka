/*******************************************************************

  Snake game with functional realization on MicroZed based MZ_APO
  board designed by Petr Porazil at PiKRON.

* main.c - main file where all thread starting
* snake_movement.h - all job with the user snake (without easy-AI)
* snake_movement_AI.h - all job with the AI snake (with easy-AI)
* output_styles.h - just some custom debug/info/error outputs
* menu.h - all job with the menu
* LCD.h - some simple functions for the the work with LSD
* mzapo_parlcd.h - parallel-connected LCD low-level access
* LED.h - contain a few mods for led strip blinking
* keyboard_input.h - here located keyboard non-block input thread
* food_job.h some simple functions for the work with the food for the snake
* mutex_get_set.h - file with all get and set functions

  (C) Copyright 2020 by Voronov Serhii, Moroz Artem
      e-mail:   voronser@fel.cvut.cz, morozart@fel.cvut.cz
      homepage: https://www.linkedin.com/in/serhii-voronov/
      license:  any combination of GPL, LGPL, MPL or BSD licenses

 *******************************************************************/

#include "snake_movement.h"
/*
 * some start settings before snake game
 */
SNAKE_T main_snake= {.quit=false, .score =0, .direction=0, .pause=false, .length = 8, .food_x=0, .food_y=0, .need_food=true, .start_game_status=false , .color=WHITE_COLOUR, .game_speed=MIDDLE_SPEED};

extern int record;

bool need_quit(){
    bool quit;
    pthread_mutex_lock(&(main_snake.mtx));
    quit = main_snake.quit;
    pthread_mutex_unlock(&(main_snake.mtx));
    return quit;
}

void snake_sleep(int speed){
    struct timespec ts;
    int milliseconds = speed;
    ts.tv_sec = milliseconds / 1000;
    ts.tv_nsec = (milliseconds % 1000) * 1000000;
    nanosleep(&ts, NULL);
}

void draw_square(int x_start, int y_start){
    int16_t color = get_snake_color();
    //all points of the square to the display array
    for (int i = x_start; i < x_start + SNAKE_SIZE_X; i++) {
        for (int j = y_start; j < y_start + SNAKE_SIZE_Y; j++) {
            display[i][j] = color;
        }
    }
}

void make_snake_bigger(){
    pthread_mutex_lock(&(main_snake.mtx));
    //increase length of the snake
    main_snake.length+=1;
    pthread_mutex_unlock(&(main_snake.mtx));
}

bool control_collision(int x, int y){
    pthread_mutex_lock(&(main_snake.mtx));
    //controls all points on the snake
    for(int i =1; i < main_snake.length-1;i++){
        if(main_snake.snake_array[i][0] == x &&
           main_snake.snake_array[i][1] == y){
                pthread_mutex_unlock(&(main_snake.mtx));
                return false;
        }
    }
    pthread_mutex_unlock(&(main_snake.mtx));
    return true;
}

void* snake_moving(void *d){
        info(SNAKE_THREAD_STARTED);
        time_t t;
        srand((unsigned) time(&t));
        int snake_len = get_snake_length();
        int snake_speed = get_game_speed();
        for(int i = 0; i < snake_len; i++){
            main_snake.snake_array[i][0] = START_SNAKE_X;
            main_snake.snake_array[i][1] = START_SNAKE_Y;
        }
        //start position for tha snake
        int x_position = START_SNAKE_X,
            y_position = START_SNAKE_Y,
            direction;

        while(!need_quit()){
            // selection of game mode for the game
            if(get_game_mode() == USER_MODE){
                direction = get_direction();
            }
            else{
                direction = AI_find_food();
            }
            //possible directions and changes for the snake head coordinates
            if      (direction == RIGHT){ x_position += SNAKE_STEP_SIZE; }
            else if (direction == UP)   { y_position -= SNAKE_STEP_SIZE; }
            else if (direction == LEFT) { x_position -= SNAKE_STEP_SIZE; }
            else if (direction == DOWN) { y_position += SNAKE_STEP_SIZE; }
            else continue;
            //"endless" screen
            if(x_position>DISPLAY_WIDTH){ x_position = 0; }
            else if(x_position<0)       { x_position = DISPLAY_WIDTH-SNAKE_STEP_SIZE; }
            if(y_position>DISPLAY_HEIGHT){ y_position = 0; }
            else if(y_position<0)       { y_position = DISPLAY_HEIGHT-SNAKE_STEP_SIZE; }

            snake_len = get_snake_length();
            //if game overs with winning
            if (snake_len==MAK_SNAKE_LENGTH){
                set_blink(WIN_BLINKING);
                win_string();
            }
            //BODY POSITION SET:
            for(int i = snake_len; i > 0; i--){
                main_snake.snake_array[i][0] = main_snake.snake_array[i-1][0];
                main_snake.snake_array[i][1] = main_snake.snake_array[i-1][1];
            }
            //HEAD POSITION SET:
            main_snake.snake_array[0][0] = x_position;
            main_snake.snake_array[0][1] = y_position;
            //end of the game with defeat
            if (!control_collision(x_position, y_position)) {
                    set_blink(DEFEAT_BLINKING);
                    lose_string();
                    info(YOU_LOST);
                    printf(" %d\n", get_snake_score());
                    //set_quit();
                    
                    if(get_snake_score() > record){
                        record = get_snake_score();
                    }

                    main_snake.score =0;
                    main_snake.direction=0;
                    main_snake.pause=false;
                    main_snake.length = 8;
                    main_snake.food_x=0;
                    main_snake.food_y=0;
                    main_snake.need_food=true;
                    main_snake.start_game_status=false;

                    goto_to_main_menu();
                    break;
            }

            clear_display();
            // get new food
            if(main_snake.need_food){ set_food(); }
            //function draws food on the screen
            draw_food(get_food_pos(X_FOOD), get_food_pos(Y_FOOD));

            //Here we drawing the whole snake:
            for(int i = 0; i < snake_len; i++){
                draw_square(main_snake.snake_array[i][1], main_snake.snake_array[i][0]);
                if(!main_snake.need_food)
                    check_food(y_position, x_position, get_food_pos(X_FOOD), get_food_pos(Y_FOOD));
            }
            frame2lcd();
            snake_sleep(snake_speed);
        }
        info(SNAKE_THREAD_FINISHED);
        return NULL;
}
