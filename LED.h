#ifndef HW_06_LED_H
#define HW_06_LED_H
#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
/*
 * This enum has all possible events for LED blinking
 */
enum blinking_mode{NONE, EAT_FOOD, START_GAME, WIN_BLINKING, DEFEAT_BLINKING};
/*
 * This function we use to activate RGB leds
 * first variable is responsible for red colour in RGB
 * second variable is responsible for green colour in RGB
 * third variable is responsible for blue colour in RGB
 * fourth variable is the number of rgb led
 */
void rgb_led(uint8_t red, uint8_t green, uint8_t blue, int LED_NUM) ;
/*
 * function makes some animation with rgb leds for win event
 */
void win_rgb_blinking();
/*
 * function makes some animation with rgb leds for lose event
 */
void loose_rgb_blinking();
/*
 * function makes some animation with rgb leds for start game event
 */
void start_game_rgb_blinking();
/*
 * function makes some animation with rgb leds when program starts
 */
void program_begin_rgb_blinking();
/*
 * functions makes some animation with led strip and rgb leds for win event
 */
void win_blinking();
/*
 * functions makes some animation with led strip and rgb leds for defeat event
 */
void defeat_blinking();
/*
 * functions makes some animation with led strip and rgb leds for start game event
 */
void start_game_blinking();
/*
 * functions makes some animation with led strip and rgb leds for eat food event
 */
void ate_food_blink();
/*
 * function for thread to control all animation of leds
 * takes some void pointer
 */
void* blinking_action(void *d);
/*
 * function helps us to know which blinking mode is setted now
 */
int get_blinking_mode();
/*
 * function takes some event and sets some blinking mode according to received event
 * variable mode is received event
 */
void set_blink(int mode );
#endif //HW_06_LED_H
