/*******************************************************************

  Snake game with functional realization on MicroZed based MZ_APO
  board designed by Petr Porazil at PiKRON.

* main.c - main file where all thread starting
* snake_movement.h - all job with the user snake (without easy-AI)
* snake_movement_AI.h - all job with the AI snake (with easy-AI)
* output_styles.h - just some custom debug/info/error outputs
* menu.h - all job with the menu
* LCD.h - some simple functions for the the work with LSD
* mzapo_parlcd.h - parallel-connected LCD low-level access
* LED.h - contain a few mods for led strip blinking
* keyboard_input.h - here located keyboard non-block input thread
* food_job.h some simple functions for the work with the food for the snake
* mutex_get_set.h - file with all get and set functions

  (C) Copyright 2020 by Voronov Serhii, Moroz Artem
      e-mail:   voronser@fel.cvut.cz, morozart@fel.cvut.cz
      homepage: https://www.linkedin.com/in/serhii-voronov/
      license:  any combination of GPL, LGPL, MPL or BSD licenses

 *******************************************************************/

#define _POSIX_C_SOURCE 200112L

#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <pthread.h>

#include "LCD.h"
#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "menu.h"
#include "LED.h"
#include "snake_movement.h"
#include "keyboard_input.h"

void mutex_cond_init(void){
    // initialize mutex window:
    pthread_mutex_init(&main_snake.mtx, NULL);
    // initialize condition:
    pthread_cond_init(&main_snake.cond, NULL);
}


int main(int argc, char *argv[]){
    info(WELCOM_FRASE);

    //preparation for lcd screen
    parlcd_mem_base = map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);
    if (parlcd_mem_base == NULL) exit(1);
    parlcd_hx8357_init(parlcd_mem_base);
    unsigned char *mem_base;
    mem_base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);
    if (mem_base == NULL) exit(1);

    ate_food_blink();

    clear_display();
    mutex_cond_init();

    pthread_t keyboard_thread, snake_moving_thread, blinking_thread;
    void* thrd_data[3] = {};
    if(pthread_create(&keyboard_thread, NULL, keyboard_thread_func, &thrd_data[0]) != 0){
        error(KEYBOARD_THREAD_ERROR);
    }
    while (get_start_game_status() == false){
        if(need_quit()){
            info(GAME_STOPPED_USER);
            return 0;
        }
    }
    if(pthread_create(&blinking_thread, NULL, blinking_action, &thrd_data[1]) != 0){
        error(BLINK_THREAD_ERROR);
    }

    while(!need_quit()){
        while (get_start_game_status() == false && !need_quit());
        if (pthread_create(&snake_moving_thread, NULL, snake_moving, &thrd_data[2]) != 0) {
            error(SNAKE_THREAD_ERROR);
        }
        while (get_start_game_status() == true && !need_quit());
    }

    pthread_join(keyboard_thread, NULL);
    pthread_join(blinking_thread, NULL);
    pthread_join(snake_moving_thread, NULL);
    info(JOB_DONE);
    return 0;
}
