#ifndef HW_06_OUTPUT_STYLES_H
#define HW_06_OUTPUT_STYLES_H

#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>

#define DEBUG_MODE false
//info:
#define WELCOM_FRASE "WELCOME TO THE SNAKE GAME\n"
#define GAME_STOPPED_USER "Game was stopped by user\n"
#define JOB_DONE "JOB DONE. Goodbye world\n"
#define YOU_LOST "You lost! Your score: "
#define PRESS_ENTER "PRESS ENTER"
#define SNAKE_THREAD_STARTED "SNAKE THREAD STARTED\n"
#define SNAKE_THREAD_FINISHED "SNAKE THREAD FINISHED\n"
#define KEYBOARD_THREAD_STARTED "KEYBOARD THREAD STARTED\n"
#define KEYBOARD_THREAD_FINISHED "KEYBOARD THREAD FINISHED\n"
#define BLINK_THREAD_STARTED "BLINK THREAD HAD STARTED\n"
#define BLINK_THREAD_FINISHED "BLINK THREAD FINISHED\n"
#define USER_PRESSED_ENTER "USER PRESSED BUTTON ENTER\n"

//debug:
#define LOW_SPEED_SET "LOW SPEED WAS SET\n"
#define MIDDLE_SPEED_SET "LOW SPEED WAS SET\n"
#define HIGH_SPEED_SET "LOW SPEED WAS SET\n"
#define GOT_UP_BUTTON "GOT UP BUTTON\n"
#define GOT_LEFT_BUTTON "GOT LEFT BUTTON\n"
#define GOT_RIGHT_BUTTON "GOT RIGHT BUTTON\n"
#define GOT_DOWN_BUTTON "GOT DOWN BUTTON\n"
#define SET_DIRECTION_NOT_ALLOWED "SET DIRECTION NOT ALLOWED. (180º rotation is forbidden)\n"

//error:
#define KEYBOARD_THREAD_ERROR "Some error in keyboard thread creating"
#define BLINK_THREAD_ERROR "Some error in blinking thread creating"
#define SNAKE_THREAD_ERROR "Some error in snake thread creating"

//MENU:
//main block:
#define START_GAME_MENU "START GAME\0"
#define SETTINGS_GAME_MENU "SETTINGS\0"
#define RECORD_GAME_MENU "USER RECORD\0"
#define EXIT_GAME_MENU "EXIT\0"
//settings:
#define BACK_SETTINGS_MENU "BACK\0"
#define COLOR_SETTINGS_MENU "SET COLOUR\0"
#define SPEED_SETTINGS_MENU "SET SPEED\0"
//record
#define RECORD_MENU "RECORD - XXX\0"
//speed settings:
#define LOW_LEVEL_MENU "LOW\0"
#define MEDIUM_LEVEL_MENU "MEDIUM\0"
#define HIGH_LEVEL_MENU "HIGH\0"
//color settings:
#define RED_COLOUR_MENU "RED COLOUR\0"
#define GREEN_COLOUR_MENU "GREEN COLOR\0"
#define WHITE_COLOUR_MENU "WHITE COLOR\0"
//game modes:
#define BACK_MENU "BACK\0"
#define USER_MODE_MENU "USER MODE\0"
#define AI_MODE_MENU "AI MODE\0"
//game outputs:
#define YOU_LOSE_LABEL "YOU LOSE\0"
#define YOU_WINNER_LABEL "YOU ARE WINNER\n YOU REACHED MAXIMUM\n\0"
#define BYE_LABEL "GOODBYE!\n\0"


/*INFO output function*/
void info(const char* line);

/*DEBUG output function*/
void debug(const char* line);

/*ERROR output function*/
void error(const char* line);

#endif //HW_06_OUTPUT_STYLES_H
