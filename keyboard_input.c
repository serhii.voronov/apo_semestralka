/*******************************************************************

  Snake game with functional realization on MicroZed based MZ_APO
  board designed by Petr Porazil at PiKRON.

* main.c - main file where all thread starting
* snake_movement.h - all job with the user snake (without easy-AI)
* snake_movement_AI.h - all job with the AI snake (with easy-AI)
* output_styles.h - just some custom debug/info/error outputs
* menu.h - all job with the menu
* LCD.h - some simple functions for the the work with LSD
* mzapo_parlcd.h - parallel-connected LCD low-level access
* LED.h - contain a few mods for led strip blinking
* keyboard_input.h - here located keyboard non-block input thread
* food_job.h some simple functions for the work with the food for the snake
* mutex_get_set.h - file with all get and set functions

  (C) Copyright 2020 by Voronov Serhii, Moroz Artem
      e-mail:   voronser@fel.cvut.cz, morozart@fel.cvut.cz
      homepage: https://www.linkedin.com/in/serhii-voronov/
      license:  any combination of GPL, LGPL, MPL or BSD licenses

 *******************************************************************/

#include "keyboard_input.h"
//struct that represents all menu(main menu, settings menu, choose colour...)
typedef struct {
    bool in_main_menu;
    bool in_set_menu;
    int menu_item;
    int items_number;
    bool in_colour_menu;
    bool in_speed_menu;
    bool in_game_mod_menu;
    bool in_record_menu;
}button_menu_t;

button_menu_t button_menu = {.menu_item = 0, .in_main_menu = true, .in_colour_menu = false,
                             .in_game_mod_menu = false, .in_set_menu = false, .in_speed_menu = false};

void call_termios(int reset){
    static struct termios tio, tioOld;
    tcgetattr(STDIN_FILENO, &tio);
    if (reset) {
        tcsetattr(STDIN_FILENO, TCSANOW, &tioOld);
    }
    else {
        tioOld = tio; //backup
        cfmakeraw(&tio);
        tio.c_oflag |= OPOST;
        tcsetattr(STDIN_FILENO, TCSANOW, &tio);
    }
}

bool get_start_game_status(){
    int game_status;
    pthread_mutex_lock(&(main_snake.mtx));
    game_status = main_snake.start_game_status;
    pthread_mutex_unlock(&(main_snake.mtx));
    return game_status;
}

void goto_to_main_menu(){
    button_menu.in_main_menu = true;
    button_menu.menu_item = 0;
}

void button_up_down(int button){
    button_menu.items_number = button_menu.in_main_menu ? ITEMS_MENU_NUMBER : ITEMS_SECONDARY_NUMBER;
    if (button == BUTTON_DOWN){
        //if button down
        button_menu.menu_item = (button_menu.menu_item + 1) % button_menu.items_number;
        debug(GOT_DOWN_BUTTON);
    }
    else{
        // if button up
        debug(GOT_UP_BUTTON);
        button_menu.menu_item --;
        if (button_menu.menu_item < 0){
            button_menu.menu_item = button_menu.items_number - 1;
        }
    }
    clear_display();
    //moving in menues
    if (button_menu.in_main_menu) {
        start_menu();
        highlight_main_menu(button_menu.menu_item);
    }
    else if (button_menu.in_game_mod_menu){
        game_mode_menu();
        highlight_game_menu(button_menu.menu_item);
    }
    else if (button_menu.in_set_menu){
        settings_menu();
        highlight_set_menu(button_menu.menu_item);
    }
    else if (button_menu.in_colour_menu){
        choose_colour();
        highlight_colour_menu(button_menu.menu_item);
    }
    else if (button_menu.in_speed_menu){
        choose_speed();
        highlight_speed_menu(button_menu.menu_item);
    }
}

void button_enter(){
    debug(USER_PRESSED_ENTER);
    // we are in main menu
    if (button_menu.in_main_menu){
        button_menu.in_main_menu = false;
        if (button_menu.menu_item == 0){
            button_menu.in_game_mod_menu = true;
            clear_display();
            game_mode_menu();
            highlight_game_menu(button_menu.menu_item);
        }
        // go to settings menu
        else if(button_menu.menu_item == 1){
            button_menu.menu_item = 0;
            button_menu.in_set_menu = true;
            clear_display();
            settings_menu();
            highlight_set_menu(button_menu.menu_item);
        }
        else if(button_menu.menu_item == 2){
            button_menu.menu_item = 0;
            button_menu.in_record_menu = true;
            clear_display(); 
            record_menu();
            //settings_menu();
            highlight_set_menu(button_menu.menu_item);
        }
    }
    // we are in settings menu
    else if (button_menu.in_set_menu){
        button_menu.in_set_menu = false;
        if (button_menu.menu_item == 2){
            button_menu.in_speed_menu = true;
            clear_display();
            choose_speed();
            highlight_speed_menu(button_menu.menu_item);
        }
        else if(button_menu.menu_item == 1){
            button_menu.in_colour_menu = true;
            clear_display();
            choose_colour();
            highlight_colour_menu(button_menu.menu_item);
        }
        //back to main menu
        else if (button_menu.menu_item == 0){
            button_menu.in_main_menu = true;
            clear_display();
            start_menu();
            highlight_main_menu(button_menu.menu_item);
        }
        button_menu.menu_item = 0;
    }
    // we are in game mode menu
    else if (button_menu.in_game_mod_menu){
        if (button_menu.menu_item == 2){
            set_game_mode(AI_MODE);
            set_start_game_status(true);
            button_menu.in_game_mod_menu = false;
        }
        else if (button_menu.menu_item == 1){
            set_game_mode(USER_MODE);
            set_start_game_status(true);
            button_menu.in_game_mod_menu = false;
        }
        //back to main menu
        else if(button_menu.menu_item == 0){
            button_menu.in_game_mod_menu = false;
            button_menu.in_main_menu = true;
            button_menu.menu_item = 0;
            clear_display();
            start_menu();
            highlight_main_menu(button_menu.menu_item);
        }
    }
    // we are in records menu
    else if (button_menu.in_record_menu){
        button_menu.in_record_menu = false;
        button_menu.in_main_menu = true;
        button_menu.menu_item = 0;
        clear_display();
        start_menu();
        highlight_main_menu(button_menu.menu_item);
    }
    // we are in colour menu
    else if (button_menu.in_colour_menu){
        //choose colour
        if (button_menu.menu_item == 0){ set_snake_color(RED_COLOUR); }
        else if (button_menu.menu_item == 1){ set_snake_color(GREEN_COLOUR);}
        else if(button_menu.menu_item == 2){set_snake_color(WHITE_COLOUR);}
        button_menu.in_colour_menu = false;
        button_menu.in_set_menu = true;
        clear_display();
        button_menu.menu_item = 0;
        settings_menu();
        highlight_set_menu(button_menu.menu_item);
    }
    // we are in speed menu
    else if (button_menu.in_speed_menu){
        //choose speed
        if (button_menu.menu_item == 0)     {
            set_game_speed(LOW_SPEED);
            debug(LOW_SPEED_SET);
        }
        else if (button_menu.menu_item == 1){
            set_game_speed(MIDDLE_SPEED);
            debug(MIDDLE_SPEED_SET);
        }
        else if (button_menu.menu_item == 2){
            set_game_speed(HIGH_SPEED);
            debug(HIGH_SPEED_SET);
        }
        button_menu.in_speed_menu = false;
        button_menu.in_set_menu = true;
        clear_display();
        button_menu.menu_item = 0;
        settings_menu();
        highlight_set_menu(button_menu.menu_item);
    }
}

void* keyboard_thread_func(void *d){
    info(KEYBOARD_THREAD_STARTED);
    button_menu.menu_item = 0;
    call_termios(0);
    clear_display();
    start_menu();
    highlight_main_menu(button_menu.menu_item);
    int input_char;
    while(!need_quit() && (input_char=getchar()) != ABORT_BUTTON ){
        int current_direction = get_direction();
        //before game starts we are in menu
        if (get_start_game_status() == false) {
            if (input_char == BUTTON_DOWN || input_char == BUTTON_UP) {
                button_up_down(input_char);
            }
            else if (input_char == ENTER_ITEM_BUTTON) {
                button_enter();
            }
        }
        //after game starts
        else {
            if(need_quit())break;
            if ((current_direction == UP && input_char == BUTTON_DOWN) ||
                (current_direction == RIGHT && input_char == BUTTON_LEFT) ||
                (current_direction == DOWN && input_char == BUTTON_UP) ||
                (current_direction == LEFT && input_char == BUTTON_RIGHT)) {
                     debug(SET_DIRECTION_NOT_ALLOWED);
            } else {
                // reactions for buttons
                if (input_char == BUTTON_UP) {
                    set_direction(UP);
                    debug(GOT_UP_BUTTON);
                } else if (input_char == BUTTON_LEFT) {
                    set_direction(LEFT);
                    debug(GOT_LEFT_BUTTON);
                } else if (input_char == BUTTON_RIGHT) {
                    set_direction(RIGHT);
                    debug(GOT_RIGHT_BUTTON);
                } else if (input_char == BUTTON_DOWN) {
                    set_direction(DOWN);
                    debug(GOT_DOWN_BUTTON);
                }
            }
        }
    }
    info(KEYBOARD_THREAD_FINISHED);
    set_quit();
    call_termios(1);
    return NULL;
}
