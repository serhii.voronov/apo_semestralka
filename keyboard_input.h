#ifndef HW_06_KEYBOARD_INPUT_H
#define HW_06_KEYBOARD_INPUT_H

#include <termios.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include <stdbool.h>
#include <pthread.h>

#include "snake_movement.h"
#include "output_styles.h"
#include "menu.h"
#include "snake_movement.h"

#define ABORT_BUTTON 'q'
#define BUTTON_UP 65
#define BUTTON_LEFT 68
#define BUTTON_RIGHT 67
#define BUTTON_DOWN 66
#define ENTER_ITEM_BUTTON 13
/*
 * function that returns current game status
 * true if game was started
 * else false
 */
bool get_start_game_status();
/*
 * function that becomes a thread and controls all keyboard actions
 * takes some void pointer
 */
void* keyboard_thread_func(void *d);
/*
 * function reacts only for up or down button
 * takes 1 argument
 */
void button_up_down(int button);
/*
 * * function reacts only for enter button
 */
void button_enter();
void goto_to_main_menu();


#endif //HW_06_KEYBOARD_INPUT_H
