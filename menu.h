#include "LCD.h"
#include "output_styles.h"

#ifndef APO_MENU_H
#define APO_MENU_H


#define ITEMS_MENU_NUMBER 4
#define ITEMS_SECONDARY_NUMBER 3

/*
 * Function prints all items of start menu to the screen
 * 1. Start new game
 * 2. Settings
 * 3. Records
 * 4. Exit
 */
void start_menu();

/*
 * Function prints all items of settings menu to the screen
 * 1. Back to the start menu
 * 2. Choose colour for snake
 * 3. Choose speed for game
 */
void settings_menu();

/*RECORD*/
void record_menu();

/*
 * Function prints all items of speed menu to the screen
 * 1. Low game speed
 * 2. Medium game speed
 * 3. High game speed
 * After your choose you returns to the settings menu
 */
void choose_speed();

/*
 * Function prints all items of colour menu to the screen
 * 1. Red colour
 * 2. Green colour
 * 3. White colour
 * After your choose you returns to the settings menu
 */
void choose_colour();

/*
 * Function highlites one item from settings menu
 * takes variable item that means the number od needed item
 */
void highlight_set_menu(int item);

/*
 * Function highlites one item from start menu
 * takes variable item that means the number od needed item
 */
void highlight_main_menu(int item);

/*
 * Function highlites one item from colour menu
 * takes variable item that means the number od needed item
 */
void highlight_colour_menu(int item);
/*
 * Function highlites one item from game menu
 * takes variable item that means the number od needed item
 */
void highlight_game_menu(int item);
/*
 * Function prints all items of game mode menu to the screen
 * 1. Back to the settings menu
 * 2. Choose colour for snake
 * 3. Choose speed for game
 */
void game_mode_menu();
/*
 * Function prints some string to the screen at the end of the win game
 */
void win_string();
/*
 * * Function prints some string to the screen at the end of the program
 */
void end_program_string();
/*
 * * Function prints some string to the screen at the end of the lost game
 */
void lose_string();
/*
 * Function highlites one item from speed menu
 * takes variable item that means the number od needed item
 */
void highlight_speed_menu(int item);

#endif //APO_MENU_H
