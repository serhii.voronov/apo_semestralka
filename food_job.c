/*******************************************************************

  Snake game with functional realization on MicroZed based MZ_APO
  board designed by Petr Porazil at PiKRON.

* main.c - main file where all thread starting
* snake_movement.h - all job with the user snake (without easy-AI)
* snake_movement_AI.h - all job with the AI snake (with easy-AI)
* output_styles.h - just some custom debug/info/error outputs
* menu.h - all job with the menu
* LCD.h - some simple functions for the the work with LSD
* mzapo_parlcd.h - parallel-connected LCD low-level access
* LED.h - contain a few mods for led strip blinking
* keyboard_input.h - here located keyboard non-block input thread
* food_job.h some simple functions for the work with the food for the snake
* mutex_get_set.h - file with all get and set functions

  (C) Copyright 2020 by Voronov Serhii, Moroz Artem
      e-mail:   voronser@fel.cvut.cz, morozart@fel.cvut.cz
      homepage: https://www.linkedin.com/in/serhii-voronov/
      license:  any combination of GPL, LGPL, MPL or BSD licenses

 *******************************************************************/

#include "food_job.h"

int generate_random(int upper){
    time_t t;
    srand((unsigned) time(&t));
    int n = (rand()) % upper;
    return n;
}

void draw_food(int x, int y){
    //food size is the size of the square
    for(int i =x; i < x+FOOD_SIZE; i++){
        for(int j =y; j < y+FOOD_SIZE; j++){
            display[i][j]=RED_COLOUR;
        }
    }
}

bool in_food_range(int x, int y, int x_food, int y_food){
    //if snakes head is in range of the food
    if((x>=x_food && x<=x_food+FOOD_SIZE) &&
       (y>=y_food && y<=y_food+FOOD_SIZE)){
        return true;
    }
    return false;
}

bool check_uniq(int x_food, int y_food){
    bool result=true;
    pthread_mutex_lock(&(main_snake.mtx));
    for (int i =0; i<main_snake.length; i++){
        // false if food was generated on the snake
        if((main_snake.snake_array[i][0] <= x_food) && (x_food <= main_snake.snake_array[i][0] + FOOD_SIZE) &&
           (main_snake.snake_array[i][1] <= y_food) && (y_food <= main_snake.snake_array[i][1] + FOOD_SIZE)){
            pthread_mutex_unlock(&(main_snake.mtx));
            return false;
        }
    }
    pthread_mutex_unlock(&(main_snake.mtx));
    return result;
}

void set_food(){
    bool uniq_coords = false;
    int x_food_tmp=0;
    int y_food_tmp=0;
    while(!uniq_coords){
        x_food_tmp= generate_random(DISPLAY_HEIGHT - FOOD_SIZE);
        y_food_tmp= generate_random(DISPLAY_WIDTH - FOOD_SIZE);
        uniq_coords=check_uniq(x_food_tmp,y_food_tmp);
    }
    pthread_mutex_lock(&(main_snake.mtx));
    //food was generated
    main_snake.need_food=false;
    //save food coordinates
    main_snake.food_x = x_food_tmp;
    main_snake.food_y = y_food_tmp;
    pthread_mutex_unlock(&(main_snake.mtx));

}
void check_food(int x_snake, int y_snake, int x_food, int y_food){
    //CHECK IF WE HAVE GOT FOOD
    if( in_food_range( x_snake,  y_snake,  x_food,  y_food) ||
        in_food_range( x_snake+SNAKE_SIZE_X,  y_snake,  x_food,  y_food) ||
        in_food_range( x_snake,  y_snake+SNAKE_SIZE_X,  x_food,  y_food)||
        in_food_range( x_snake+SNAKE_SIZE_X,  y_snake+SNAKE_SIZE_X,  x_food,  y_food)){
        //led animation
        set_blink(EAT_FOOD);
        //increase snake length
        make_snake_bigger();
        //food was eaten and we need the new one
        main_snake.need_food = true;
        increase_score();
    }
}
