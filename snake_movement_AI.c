/*******************************************************************

  Snake game with functional realization on MicroZed based MZ_APO
  board designed by Petr Porazil at PiKRON.

* main.c - main file where all thread starting
* snake_movement.h - all job with the user snake (without easy-AI)
* snake_movement_AI.h - all job with the AI snake (with easy-AI)
* output_styles.h - just some custom debug/info/error outputs
* menu.h - all job with the menu
* LCD.h - some simple functions for the the work with LSD
* mzapo_parlcd.h - parallel-connected LCD low-level access
* LED.h - contain a few mods for led strip blinking
* keyboard_input.h - here located keyboard non-block input thread
* food_job.h some simple functions for the work with the food for the snake
* mutex_get_set.h - file with all get and set functions

  (C) Copyright 2020 by Voronov Serhii, Moroz Artem
      e-mail:   voronser@fel.cvut.cz, morozart@fel.cvut.cz
      homepage: https://www.linkedin.com/in/serhii-voronov/
      license:  any combination of GPL, LGPL, MPL or BSD licenses

 *******************************************************************/

#include "snake_movement_AI.h"
#include "mutex_get_set.h"

bool check_move(int new_move_direction, int current_move_direction){
    bool ret = true;
    int coord_snake_x_aftermove = main_snake.snake_array[0][0];
    int coord_snake_y_aftermove = main_snake.snake_array[0][1];
    //what will happens with snakes heads coordinates in different directions
    if (new_move_direction == RIGHT){
        coord_snake_x_aftermove += SNAKE_STEP_SIZE ;
    }
    else if (new_move_direction == DOWN){
        coord_snake_y_aftermove += SNAKE_STEP_SIZE;
    }
    else if (new_move_direction == LEFT){
        coord_snake_x_aftermove -= SNAKE_STEP_SIZE ;
    }
    else if (new_move_direction == UP){
        coord_snake_y_aftermove -= SNAKE_STEP_SIZE;
    }
    //here we controls all snake coordinates with all possible directions
    for (int i = 0; i < main_snake.length; i ++){
        if (current_move_direction == UP && new_move_direction == UP){
            if (main_snake.snake_array[i][1] < coord_snake_y_aftermove && coord_snake_y_aftermove< main_snake.snake_array[i][1] + SNAKE_SIZE_Y){
                ret = false;
                break;
            }
        }
        else if (current_move_direction == UP && (new_move_direction == RIGHT || new_move_direction == LEFT)){
            if (main_snake.snake_array[i][0] < coord_snake_x_aftermove && coord_snake_x_aftermove < main_snake.snake_array[i][0] + SNAKE_SIZE_X) {
                ret = false;
                break;
            }
        }
        else if ((current_move_direction == DOWN && new_move_direction == DOWN)){
            if(coord_snake_y_aftermove < main_snake.snake_array[i][1] && coord_snake_y_aftermove > main_snake.snake_array[i][1] + SNAKE_SIZE_X){
                ret = false;
                break;
            }
        }
        else if (current_move_direction == DOWN && (new_move_direction == RIGHT || new_move_direction == LEFT)){
            if (main_snake.snake_array[i][0] < coord_snake_x_aftermove && coord_snake_x_aftermove < main_snake.snake_array[i][0] + SNAKE_SIZE_X){
                ret = false;
                break;
            }
        }
        else if (current_move_direction == RIGHT && new_move_direction == RIGHT){
            if (main_snake.snake_array[i][0] < coord_snake_x_aftermove && coord_snake_x_aftermove< main_snake.snake_array[i][0] + SNAKE_SIZE_X){
                ret = false;
                break;
            }
        }
        else if (current_move_direction == RIGHT && (new_move_direction == UP || new_move_direction == DOWN)){
            if ( main_snake.snake_array[i][1] < coord_snake_y_aftermove && coord_snake_y_aftermove< main_snake.snake_array[i][1] + SNAKE_SIZE_X){
                ret = false;
                break;
            }
        }
        else if (current_move_direction == LEFT && new_move_direction == LEFT){
            if (main_snake.snake_array[i][0] < coord_snake_x_aftermove && coord_snake_x_aftermove < main_snake.snake_array[i][0] + SNAKE_SIZE_X){
                ret = false;
                break;
            }
        }
        else if (current_move_direction == LEFT && (new_move_direction == UP || new_move_direction == DOWN)){
            if (main_snake.snake_array[i][1] < coord_snake_y_aftermove && coord_snake_y_aftermove< main_snake.snake_array[i][1] + SNAKE_SIZE_X){
                ret = false;
                break;
            }
        }
    }
    return ret;
}

int AI_find_food(){
    int current_direction = get_direction();
    int difference_x = get_food_pos(Y_FOOD) - main_snake.snake_array[0][0];
    int difference_y = get_food_pos(X_FOOD) - main_snake.snake_array[0][1];
    int ret;
    //all possible moves for snake when current directions is RIGHT
    if (current_direction == RIGHT && (difference_x > 0 || difference_x < 0)&& abs(difference_x) > SNAKE_SIZE_X ) {
        if (difference_x < 0 && abs(difference_x) < DISPLAY_WIDTH/2) {
            if (difference_y > 0 && check_move(DOWN, RIGHT)){
                ret = DOWN;
            }
            else if (difference_y < 0 && check_move(UP,RIGHT)){
                ret = UP;
            }
            else {
                ret = RIGHT;
            }
        }
        else { ret = RIGHT;}
    }
    else if (current_direction == RIGHT && abs(difference_x) < SNAKE_SIZE_X && difference_y > 0){
        int another_path = main_snake.snake_array[0][1] + (DISPLAY_HEIGHT - get_food_pos(X_FOOD));
        if (another_path < abs(difference_y) && check_move(UP,RIGHT)){
            ret = UP;
        }
        else if (another_path > abs(difference_y) && check_move(DOWN, RIGHT)){
            ret = DOWN;
        }
        else{
            ret = RIGHT;
        }
    }
    else if (current_direction == RIGHT && abs(difference_x) < SNAKE_SIZE_X && difference_y < 0 ){
        int another_path = get_food_pos(X_FOOD) + (DISPLAY_HEIGHT - main_snake.snake_array[0][1]);
        if (another_path < abs(difference_y) && check_move(DOWN, RIGHT)){
            ret = DOWN;
        }
        else if (another_path > abs(difference_y) && check_move(UP, RIGHT)){
            ret = UP;
        }
        else{
            ret = RIGHT;
        }
    }
        //all possible moves for snake when current directions is UP
    else if (current_direction == UP && abs(difference_x) < SNAKE_SIZE_X && abs(difference_y) > SNAKE_SIZE_X){
        if (check_move(UP, UP)) {
            ret = UP;
        } else if (check_move(LEFT, UP)){
            ret = LEFT;
        } else{
            ret = RIGHT;
        }
    }
    else if (current_direction == UP && difference_x > 0 && abs(difference_y) < SNAKE_SIZE_X){
        int another_path = main_snake.snake_array[0][0] + (DISPLAY_WIDTH - get_food_pos(Y_FOOD) );
        if (another_path < abs(difference_x) && check_move(LEFT, UP)){
            ret = LEFT;
        }
        else if (another_path > abs(difference_x) && check_move(RIGHT, UP)){
            ret = RIGHT;
        }
        else{
            ret = UP;
        }
    }
    else if (current_direction == UP && difference_x < 0 && abs(difference_y) < SNAKE_SIZE_X){
        int another_path = get_food_pos(Y_FOOD) + (DISPLAY_WIDTH - main_snake.snake_array[0][0] );
        if (another_path < abs(difference_x) && check_move(RIGHT, UP)){
            ret = RIGHT;
        }
        else if (another_path > abs(difference_x) && check_move(LEFT, UP)){ ret = LEFT; }
        else{
            ret = UP;
        }
    }
        //all possible moves for snake when current directions is DOWN
    else if (current_direction == DOWN && abs(difference_x) < SNAKE_SIZE_X && abs(difference_y) > SNAKE_SIZE_X){
        if (check_move(DOWN, DOWN)) {
            ret = DOWN;
        }
        else if(check_move(RIGHT, DOWN)) {
            ret = RIGHT;
        } else{
            ret = LEFT;
        }
    }
    else if (current_direction == DOWN && difference_x > 0 && abs(difference_y) < SNAKE_SIZE_X){
        int another_path = main_snake.snake_array[0][0] + (DISPLAY_WIDTH - get_food_pos(Y_FOOD) );
        if (another_path < abs(difference_x) && check_move(LEFT, DOWN)){ ret = LEFT; }
        else if (another_path > abs(difference_x) && check_move(RIGHT, DOWN)){
            ret = RIGHT;
        }
        else{
            ret = DOWN;
        }
    }
    else if (current_direction == DOWN && difference_x < 0 && abs(difference_y) < SNAKE_SIZE_X){
        int another_path = get_food_pos(Y_FOOD) + (DISPLAY_WIDTH - main_snake.snake_array[0][0]);
        if (another_path < abs(difference_x) && check_move( RIGHT, DOWN)){
            ret = RIGHT;
        }
        else if ( another_path > abs(difference_x) && check_move(LEFT, DOWN)){ ret = LEFT; }
        else{
            ret = DOWN;
        }
    }
        //all possible moves for snake when current directions is LEFT
    else if (current_direction == LEFT && (difference_x > 0 || difference_x < 0) && abs(difference_x) > SNAKE_SIZE_X){
        if (difference_x > 0 && difference_x < DISPLAY_WIDTH / 2) {
            if (difference_y > 0 && check_move(DOWN, LEFT)){ ret = DOWN; }
            else if (difference_y < 0 && check_move(UP, LEFT)){ ret = UP; }
            else{
                ret = LEFT;
            }
        }
        else { ret = LEFT; }
    }
    else if (current_direction == LEFT && abs(difference_x) < SNAKE_SIZE_X && difference_y < 0){
        int another_path = get_food_pos(X_FOOD) + (DISPLAY_HEIGHT - main_snake.snake_array[0][1]);
        if (another_path < abs(difference_y) && check_move( DOWN, LEFT)){ ret = DOWN; }
        else if (another_path > abs(difference_y) && check_move(UP, LEFT)){ ret = UP; }
        else{
            ret = LEFT;
        }
    }
    else if (current_direction == LEFT && abs(difference_x) < SNAKE_SIZE_X && difference_y > 0){
        int another_path = main_snake.snake_array[0][1] + (DISPLAY_HEIGHT - get_food_pos(X_FOOD));
        if (another_path < abs(difference_y) && check_move(UP, LEFT)){ ret = UP; }
        else if (another_path > abs(difference_y) && check_move(DOWN, LEFT)){ ret = DOWN; }
        else{
            ret = LEFT;
        }
    }
    else{ ret = get_direction(); }

    main_snake.direction = ret;
    return ret;
}
