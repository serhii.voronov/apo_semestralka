/*******************************************************************

  Snake game with functional realization on MicroZed based MZ_APO
  board designed by Petr Porazil at PiKRON.

* main.c - main file where all thread starting
* snake_movement.h - all job with the user snake (without easy-AI)
* snake_movement_AI.h - all job with the AI snake (with easy-AI)
* output_styles.h - just some custom debug/info/error outputs
* menu.h - all job with the menu
* LCD.h - some simple functions for the the work with LSD
* mzapo_parlcd.h - parallel-connected LCD low-level access
* LED.h - contain a few mods for led strip blinking
* keyboard_input.h - here located keyboard non-block input thread
* food_job.h some simple functions for the work with the food for the snake
* mutex_get_set.h - file with all get and set functions

  (C) Copyright 2020 by Voronov Serhii, Moroz Artem
      e-mail:   voronser@fel.cvut.cz, morozart@fel.cvut.cz
      homepage: https://www.linkedin.com/in/serhii-voronov/
      license:  any combination of GPL, LGPL, MPL or BSD licenses

 *******************************************************************/

#include "mutex_get_set.h"

int get_food_pos(int coord){
    int food_position = 0;
    pthread_mutex_lock(&(main_snake.mtx));
    //we want x or y coords of the food
    if     (coord==X_FOOD){ food_position = main_snake.food_x; }
    else if(coord==Y_FOOD){ food_position = main_snake.food_y; }
    pthread_mutex_unlock(&(main_snake.mtx));
    return food_position;
}
int16_t get_snake_color(){
    int16_t color ;
    pthread_mutex_lock(&(main_snake.mtx));
    color = main_snake.color;
    pthread_mutex_unlock(&(main_snake.mtx));
    return color;
}
int get_direction(){
    int direction_return;
    pthread_mutex_lock(&(main_snake.mtx));
    direction_return = main_snake.direction;
    pthread_mutex_unlock(&(main_snake.mtx));
    return direction_return;
}

int get_snake_length(){
    int length_return;
    pthread_mutex_lock(&(main_snake.mtx));
    length_return = main_snake.length;
    pthread_mutex_unlock(&(main_snake.mtx));
    return length_return;
}

int get_snake_score(){
    int score ;
    pthread_mutex_lock(&(main_snake.mtx));
    score = main_snake.score;
    pthread_mutex_unlock(&(main_snake.mtx));
    return score;
}
void set_snake_color(int16_t color){
    pthread_mutex_lock(&(main_snake.mtx));
    main_snake.color = color;
    pthread_mutex_unlock(&(main_snake.mtx));
}

void increase_score(){
    pthread_mutex_lock(&(main_snake.mtx));
    //increase the score when snake eats food
    main_snake.score++;
    info("YOU GOT FOOD! CURRENT SCORE:");
    printf("%d\n",main_snake.score);
    pthread_mutex_unlock(&(main_snake.mtx));
}
int get_game_mode(){
    int game_mode ;
    pthread_mutex_lock(&(main_snake.mtx));
    game_mode = main_snake.mode;
    pthread_mutex_unlock(&(main_snake.mtx));
    return game_mode;
}
void set_quit() {
    pthread_mutex_lock(&(main_snake.mtx));
    main_snake.quit = true;
    pthread_mutex_unlock(&(main_snake.mtx));
}

void set_direction(int direction_input){
    pthread_mutex_lock(&(main_snake.mtx));
    main_snake.direction = direction_input;
    pthread_mutex_unlock(&(main_snake.mtx));
}

void set_start_game_status(bool status_set){
    pthread_mutex_lock(&(main_snake.mtx));
    main_snake.start_game_status = status_set;
    pthread_mutex_unlock(&(main_snake.mtx));
}

void set_game_mode(int game_mode){
    pthread_mutex_lock(&(main_snake.mtx));
    main_snake.mode = game_mode;
    pthread_mutex_unlock(&(main_snake.mtx));
}

int get_game_speed(){
    int game_speed;
    pthread_mutex_lock(&(main_snake.mtx));
    game_speed = main_snake.game_speed;
    pthread_mutex_unlock(&(main_snake.mtx));
    return game_speed;
}

void set_game_speed(int game_speed){
    pthread_mutex_lock(&(main_snake.mtx));
    main_snake.game_speed = game_speed;
    pthread_mutex_unlock(&(main_snake.mtx));
}




