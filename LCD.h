#ifndef HW_06_LSD_H
#define HW_06_LSD_H

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include <inttypes.h>
#include <string.h>
#include <stdbool.h>

#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "LED.h"
#include "font_types.h"

#define WHITE_COLOUR 0xFFFF
#define BLACK_COLOUR 0x0
#define RED_COLOUR 0xF800
#define GREEN_COLOUR 0x07E0
#define DISPLAY_HEIGHT 320
#define DISPLAY_WIDTH 480

#endif //HW_06_LSD_H

extern unsigned char *parlcd_mem_base;
/*
 * 2D array represents the display
 */
extern uint16_t display [DISPLAY_HEIGHT][DISPLAY_WIDTH];
/*
 * function clears the display and paints it in black
 */
void clear_display();

/*
 * function redraws some rectangle form part of the display from some coordinate
 * takes 2 arguments
 * 1. start pixel represents the left top corner of the rectangle
 * 2. colour of redrawed part of the display
 */
void redraw_display(int start_pixel, uint16_t colour);
/*
 * function that portrays to the display
 */
void frame2lcd();
/*
 * function portray some character to the display
 * takes 5 arguments
 * 1. character that we want to portray to the display
 * 2. x coord of the character
 * 3. y coord of the character
 * 4. colour of the character
 * 5.colour of the background
 */
int char_to_display(char c, int y_axis, int x_axis, uint16_t forecolor, uint16_t backcolor);
/*
 * function portray some character to the display
 * takes 5 arguments
 * 1. pointer to the string that we want to portray to the display
 * 2. x coord of the string
 * 3. y coord of the string
 * 4. colour of the string
 * 5.colour of the background
 */
int string_to_display(char * str, int yrow, int xcolumn, uint16_t forecolor, uint16_t backcolor);


