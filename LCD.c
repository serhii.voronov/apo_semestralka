/*******************************************************************

  Snake game with functional realization on MicroZed based MZ_APO
  board designed by Petr Porazil at PiKRON.

* main.c - main file where all thread starting
* snake_movement.h - all job with the user snake (without easy-AI)
* snake_movement_AI.h - all job with the AI snake (with easy-AI)
* output_styles.h - just some custom debug/info/error outputs
* menu.h - all job with the menu
* LCD.h - some simple functions for the the work with LSD
* mzapo_parlcd.h - parallel-connected LCD low-level access
* LED.h - contain a few mods for led strip blinking
* keyboard_input.h - here located keyboard non-block input thread
* food_job.h some simple functions for the work with the food for the snake
* mutex_get_set.h - file with all get and set functions

  (C) Copyright 2020 by Voronov Serhii, Moroz Artem
      e-mail:   voronser@fel.cvut.cz, morozart@fel.cvut.cz
      homepage: https://www.linkedin.com/in/serhii-voronov/
      license:  any combination of GPL, LGPL, MPL or BSD licenses

 *******************************************************************/

#include "LCD.h"

#define SCALE_SIZE 2

uint16_t display [DISPLAY_HEIGHT][DISPLAY_WIDTH];
unsigned char *parlcd_mem_base = NULL;


void clear_display(){
    memset(display, BLACK_COLOUR, sizeof(display[0][0]) * DISPLAY_HEIGHT * DISPLAY_WIDTH);
}

void frame2lcd()
{
    // lcd pointer to 0,0
    *(volatile uint16_t*)(parlcd_mem_base + PARLCD_REG_CMD_o) = 0x2c; // command

    volatile uint32_t* ptr = (volatile uint32_t*) display;
    volatile uint32_t* dst = (volatile uint32_t*) (parlcd_mem_base + PARLCD_REG_DATA_o);

    int i;
    for(i=0; i<DISPLAY_HEIGHT*(DISPLAY_WIDTH/2);i++) *dst = *ptr++;
}

int char_to_display(char c, int y_axis, int x_axis, uint16_t forecolor, uint16_t backcolor)
{
    int cix = c,
        letter_width = 8*SCALE_SIZE,
        y,x,
        tmp_buffer_x = 0,
        tmp_buffer_y = 0;
    if(cix<0) cix=0;
    for(y = 0; y < 14; y++){
        uint16_t tmp_mask = font_rom8x16.bits[16 * cix + y];
        tmp_buffer_x = 0;
        for(x = 0; x < letter_width; x++){
            uint16_t color_this_box;
            if(tmp_mask & 0x8000) { color_this_box = forecolor; }
            else{ color_this_box=backcolor; }

            for(int jj = 0; jj < SCALE_SIZE; jj++){
                for(int kk = 0; kk<SCALE_SIZE; kk++){
                    display[y + y_axis + tmp_buffer_y + jj][x + x_axis + tmp_buffer_x + kk]=color_this_box;
                }
            }
            tmp_mask = tmp_mask << 1;
            tmp_buffer_x += SCALE_SIZE;
        }
        tmp_buffer_y+=SCALE_SIZE;
    }
    return SCALE_SIZE * letter_width;
}

void redraw_display(int start_pixel, uint16_t colour){
    for(int i = start_pixel; i < start_pixel + 50; i ++){
        for (int j = 0; j < DISPLAY_WIDTH; j ++){
            display[i][j] = colour;
        }
    }
    frame2lcd();
}


int string_to_display(char * str, int yrow, int xcolumn, uint16_t forecolor, uint16_t backcolor){
    char c; int w=0;
    while((c=*str++)!=0)
        w += char_to_display(c, yrow, xcolumn + w, forecolor, backcolor);
    return w;
}
