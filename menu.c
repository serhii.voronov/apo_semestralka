/*******************************************************************

  Snake game with functional realization on MicroZed based MZ_APO
  board designed by Petr Porazil at PiKRON.

* main.c - main file where all thread starting
* snake_movement.h - all job with the user snake (without easy-AI)
* snake_movement_AI.h - all job with the AI snake (with easy-AI)
* output_styles.h - just some custom debug/info/error outputs
* menu.h - all job with the menu
* LCD.h - some simple functions for the the work with LSD
* mzapo_parlcd.h - parallel-connected LCD low-level access
* LED.h - contain a few mods for led strip blinking
* keyboard_input.h - here located keyboard non-block input thread
* food_job.h some simple functions for the work with the food for the snake
* mutex_get_set.h - file with all get and set functions

  (C) Copyright 2020 by Voronov Serhii, Moroz Artem
      e-mail:   voronser@fel.cvut.cz, morozart@fel.cvut.cz
      homepage: https://www.linkedin.com/in/serhii-voronov/
      license:  any combination of GPL, LGPL, MPL or BSD licenses

 *******************************************************************/

#include "menu.h"

int record = 0;
/*
 * This arrays are keeping strings to print it out to the screen
*/
char str_menu_main[4][12] = {START_GAME_MENU,SETTINGS_GAME_MENU, RECORD_GAME_MENU, EXIT_GAME_MENU};
char str_menu_settings[3][20] = {BACK_SETTINGS_MENU, COLOR_SETTINGS_MENU, SPEED_SETTINGS_MENU};
char str_menu_speed[3][7] = {LOW_LEVEL_MENU, MEDIUM_LEVEL_MENU, HIGH_LEVEL_MENU};
char str_menu_record[1][13] = {RECORD_MENU};
char str_menu_colour[3][13] = {RED_COLOUR_MENU,GREEN_COLOUR_MENU, WHITE_COLOUR_MENU};
char str_menu_game_mode[3][10] = { BACK_MENU, USER_MODE_MENU, AI_MODE_MENU};
char str_end_game[3][37] = {YOU_LOSE_LABEL, YOU_WINNER_LABEL, PRESS_ENTER};
char str_end_program[1][10] = {BYE_LABEL};

void start_menu(){
    string_to_display(str_menu_main[0], 30, 30, WHITE_COLOUR, BLACK_COLOUR);
    string_to_display(str_menu_main[1], 100, 30, WHITE_COLOUR, BLACK_COLOUR);
    string_to_display(str_menu_main[2], 170, 30, WHITE_COLOUR, BLACK_COLOUR);
    string_to_display(str_menu_main[3], 240, 30, WHITE_COLOUR, BLACK_COLOUR);
    frame2lcd();
}

void highlight_main_menu(int item){
    redraw_display(30 + item * 70, RED_COLOUR);
    string_to_display(str_menu_main[item], 30 + item * 70, 30, WHITE_COLOUR, BLACK_COLOUR);
    frame2lcd();
}

void settings_menu(){
    string_to_display(str_menu_settings[0], 30, 30, WHITE_COLOUR, BLACK_COLOUR);
    string_to_display(str_menu_settings[1], 100, 30, WHITE_COLOUR, BLACK_COLOUR);
    string_to_display(str_menu_settings[2], 170, 30, WHITE_COLOUR, BLACK_COLOUR);
    frame2lcd();
}

void highlight_set_menu(int item){
    redraw_display(30 + item * 70, RED_COLOUR);
    string_to_display(str_menu_settings[item], 30 + item * 70, 30, WHITE_COLOUR, BLACK_COLOUR);
    frame2lcd();
}

void game_mode_menu(){
    string_to_display(str_menu_game_mode[0], 30, 30, WHITE_COLOUR, BLACK_COLOUR);
    string_to_display(str_menu_game_mode[1], 100, 30, WHITE_COLOUR, BLACK_COLOUR);
    string_to_display(str_menu_game_mode[2], 170, 30, WHITE_COLOUR, BLACK_COLOUR);
    frame2lcd();
}

void record_menu(){
    str_menu_record[0][11] = record % 10 + '0';
    str_menu_record[0][10] = (record / 10) % 10 + '0';
    str_menu_record[0][9] = record / 100 + '0';
    string_to_display(str_menu_record[0], 135, 30, WHITE_COLOUR, BLACK_COLOUR);
    frame2lcd();
}

void highlight_game_menu(int item){
    redraw_display(30 + item * 70, RED_COLOUR);
    string_to_display(str_menu_game_mode[item], 30 + item * 70, 30, WHITE_COLOUR, BLACK_COLOUR);
    frame2lcd();
}

void choose_speed() {
    string_to_display(str_menu_speed[0], 30, 30, WHITE_COLOUR, BLACK_COLOUR);
    string_to_display(str_menu_speed[1], 100, 30, WHITE_COLOUR, BLACK_COLOUR);
    string_to_display(str_menu_speed[2], 170, 30, WHITE_COLOUR, BLACK_COLOUR);
    frame2lcd();
}

void highlight_speed_menu(int item){
    redraw_display(30 + item * 70, RED_COLOUR);
    string_to_display(str_menu_speed[item], 30 + item * 70, 30, WHITE_COLOUR, BLACK_COLOUR);
    frame2lcd();
}

void choose_colour(){
    string_to_display(str_menu_colour[0], 30, 30, WHITE_COLOUR, BLACK_COLOUR);
    string_to_display(str_menu_colour[1], 100, 30, WHITE_COLOUR, BLACK_COLOUR);
    string_to_display(str_menu_colour[2], 170, 30, WHITE_COLOUR, BLACK_COLOUR);
    frame2lcd();
}

void highlight_colour_menu(int item){
    redraw_display(30 + item * 70, RED_COLOUR);
    string_to_display(str_menu_colour[item], 30 + item * 70, 30, WHITE_COLOUR, BLACK_COLOUR);
    frame2lcd();
}

void lose_string(){
    clear_display();
    string_to_display(str_end_game[0], 150,200 , WHITE_COLOUR, BLACK_COLOUR);
    string_to_display(str_end_game[2], 230, 30, WHITE_COLOUR, BLACK_COLOUR);
    frame2lcd();
}

void end_program_string(){
    clear_display();
    string_to_display(str_end_program[0], 150,200 , WHITE_COLOUR, BLACK_COLOUR);
    frame2lcd();
}

void win_string(){
    clear_display();
    string_to_display(str_end_game[1], 100, 30, WHITE_COLOUR, BLACK_COLOUR);
    string_to_display(str_end_game[2], 170, 30, WHITE_COLOUR, BLACK_COLOUR);
    frame2lcd();
}
