#ifndef HW_06_SNAKE_MOVEMENT_AI_H
#define HW_06_SNAKE_MOVEMENT_AI_H

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include <inttypes.h>
#include <string.h>
#include <stdbool.h>
#include <pthread.h>
#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "LED.h"
#include "food_job.h"
#include "snake_movement.h"

#include "LCD.h"

#define SLEEP_PERIOD 10000
#define SECOND_SLEEP 250000
#define SLEEP_PERIOD_DEFEAT 50000
#define LED_NUMBER 32
#define WHITE_COLOUR 0xFFFF
#define BLACK_COLOUR 0x0
#define RED_COLOUR 0xF800
#define GREEN_COLOUR 0x07E0
#define DISPLAY_HEIGHT 320
#define DISPLAY_WIDTH 480

#define SNAKE_SIZE_X 20
#define SNAKE_SIZE_Y 20

#define SNAKE_STEP_SIZE 20

#define START_SNAKE_Y 100
#define START_SNAKE_X 100
/*
 * function finds correct direction for the snake to find food and returns this direction
 */
int AI_find_food();
/*
 * function checks if snake doing move correctly without risk of collision
 * takes 2 arguments
 * 1. represents the new direction for the snake
 * 2. represents the current direction for the snake
 * returns false if risk of collision else true
 */
bool check_move(int new_move_direction, int current_move_direction);


#endif //HW_06_SNAKE_MOVEMENT_AI_H
