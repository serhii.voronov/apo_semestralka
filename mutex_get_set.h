#ifndef HW_06_MUTEX_GET_SET_H
#define HW_06_MUTEX_GET_SET_H

#include "food_job.h"
/*
 * function increases score when snake eats food
 */
void increase_score();

/*
 * function takes the argument that tells which coord of the food we want
 * return X coord of the food if argument = X_FOOD
 * return Y coord of the food if argument = Y_FOOD
 */
int get_food_pos(int coord);

/*
 * function takes ont argument that tells which colour of the snake we choose
 * and sets the snake colour for the next game
 */
void set_snake_color(int16_t color);

/*
 * function returns the current direction of the snake
 */
int get_direction();

/*
 *function returns the current direction of the snake
 */
int get_snake_length();

/*
 * function returns the current quantity of eaten food in the game
 */
int get_snake_score();

/*
 * function returns the chosen colour of the snake (red, green, white)
 */
int16_t get_snake_color();

/*
 * function returns the chosen game mode(AI mode or User mode)
 */
int get_game_mode();

/*
 * function sets quit to true
 */
void set_quit();

/*
 *  function takes some direction lake an argument and sets the
 *  current direction according to the received argument
 */
void set_direction(int direction_input);

/*
 *  function takes an argument that represents the game mode selected by user and sets it
 */
void set_game_mode(int game_mode);

/*
 * function takes the boolean argument and sets the start game status according to the argument
 * true if game was started else false
 */
void set_start_game_status(bool status_set);

/*
 * function returns game speed that was selected by user
 */
int get_game_speed();

/*
 *  *  function takes an argument that represents the game selected selected by user and sets it
 */
void set_game_speed(int game_speed);

#endif //HW_06_MUTEX_GET_SET_H
