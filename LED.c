/*******************************************************************

  Snake game with functional realization on MicroZed based MZ_APO
  board designed by Petr Porazil at PiKRON.

* main.c - main file where all thread starting
* snake_movement.h - all job with the user snake (without easy-AI)
* snake_movement_AI.h - all job with the AI snake (with easy-AI)
* output_styles.h - just some custom debug/info/error outputs
* menu.h - all job with the menu
* LCD.h - some simple functions for the the work with LSD
* mzapo_parlcd.h - parallel-connected LCD low-level access
* LED.h - contain a few mods for led strip blinking
* keyboard_input.h - here located keyboard non-block input thread
* food_job.h some simple functions for the work with the food for the snake
* mutex_get_set.h - file with all get and set functions

  (C) Copyright 2020 by Voronov Serhii, Moroz Artem
      e-mail:   voronser@fel.cvut.cz, morozart@fel.cvut.cz
      homepage: https://www.linkedin.com/in/serhii-voronov/
      license:  any combination of GPL, LGPL, MPL or BSD licenses

 *******************************************************************/


#include <stdint.h>
#include <unistd.h>
#include "mzapo_regs.h"
#include "LED.h"
#include "snake_movement.h"

#define SLEEP_PERIOD 10000
#define SECOND_SLEEP 250000
#define SLEEP_PERIOD_DEFEAT 50000
#define EATING_FOOD_PERIOD 20000
#define LED_NUMBER 32
#define RGB_SLEEP 500000

void rgb_led(uint8_t red, uint8_t green, uint8_t blue, int LED_NUM) {
    unsigned char *led_mem_base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);
    volatile uint8_t* ptr ;
    if(LED_NUM==0){ptr = led_mem_base + SPILED_REG_LED_RGB1_o;}
    else{ptr = led_mem_base + SPILED_REG_LED_RGB2_o;}
    *ptr = blue; //blue
    ptr++;
    *ptr = green; //green
    ptr++;
    *ptr = red; //red
}

void win_rgb_blinking(){
    for (int i = 0; i < 3;i ++){
        rgb_led(0,255,0,0);
        rgb_led(0,255,0,1);
        usleep(SECOND_SLEEP);
        rgb_led(0,0,0,0);
        rgb_led(0,0,0,1);
        usleep(SECOND_SLEEP);
    }
}

void loose_rgb_blinking(){
    for (int i = 0; i < 3;i ++){
        rgb_led(255,0,0,0);
        rgb_led(255,0,0,1);
        usleep(SECOND_SLEEP);
        rgb_led(0,0,0,0);
        rgb_led(0,0,0,1);
        usleep(SECOND_SLEEP);
    }
}

void program_begin_rgb_blinking(){
    for (int i = 0; i < 10; i ++){
        if (i % 2 == 0){
            rgb_led(0,0,255, 0);
        }
        else{
            rgb_led(255,255,0, 1);
        }
        usleep(SLEEP_PERIOD * 5);
        rgb_led(0,0,0,0);
        rgb_led(0,0,0,1);
        usleep(SLEEP_PERIOD * 5);
    }
    rgb_led(0,0,255, 0);
    rgb_led(255,255,0, 1);
    usleep(RGB_SLEEP);
    rgb_led(0,0,0,0);
    rgb_led(0,0,0,1);
    usleep(RGB_SLEEP);
}

void start_game_rgb_blinking(){
    for (int i = 0; i < 3; i ++){
        rgb_led(0, 0, 255, 0);
        usleep(SLEEP_PERIOD * 5);
        rgb_led(0,0,0,0);
        usleep(SLEEP_PERIOD * 5);
    }
    rgb_led(0,0,0,0);
    for (int i = 0; i < 3; i ++){
        rgb_led(0, 255, 0, 1);
        usleep(SLEEP_PERIOD * 5);
        rgb_led(0,0,0,1);
        usleep(SLEEP_PERIOD * 5);
    }
    for (int i = 0; i < 3; i ++){
        rgb_led(0, 255, 0, 1);
        rgb_led(0, 0, 255, 0);
        usleep(SLEEP_PERIOD * 5);
        rgb_led(0,0,0,0);
        rgb_led(0,0,0,1);
        usleep(SLEEP_PERIOD * 5);
    }
    rgb_led(0,0,0,1);
}

void eat_food_rgb_blinking(){
    for (int i = 0; i < 3; i ++){
        rgb_led(255, 0, 255, 0);
        rgb_led(255, 255, 0, 1);
        usleep(SLEEP_PERIOD * 5);
        rgb_led(0,0,0,0);
        rgb_led(0,0,0,1);
        usleep(SLEEP_PERIOD * 5);
    }
}

void win_blinking() {
    unsigned char *mem_base;
    mem_base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);
    //firs led form left
    unsigned int left_LED_start = 0x80000000,
    //firs led form right
                 right_LED_start = 0x00000001;
    win_rgb_blinking();
    for (int i = 0; i < LED_NUMBER/2; i ++){
        *(volatile uint32_t *) (mem_base + SPILED_REG_LED_LINE_o) = left_LED_start + right_LED_start;
        left_LED_start /= 2;
        right_LED_start *= 2;
        usleep(SLEEP_PERIOD);
    }
    //firs right led form the center
    unsigned int LED_center_first = 0x00008000,
    //firs left led form the center
                 LED_center_second = 0x00010000;
    for (int i = 0; i < LED_NUMBER/2; i ++){
        *(volatile uint32_t *) (mem_base + SPILED_REG_LED_LINE_o) = LED_center_first + LED_center_second;
        LED_center_first /= 2;
        LED_center_second *= 2;
        usleep(SLEEP_PERIOD);
    }
    for (int i = 0; i < 3; i ++) {
        // all leds are switched on
        *(volatile uint32_t *) (mem_base + SPILED_REG_LED_LINE_o) = 0xFFFFFFFF;
        usleep(SECOND_SLEEP);
        // all leds are switched off
        *(volatile uint32_t *) (mem_base + SPILED_REG_LED_LINE_o) = 0;
        usleep(SECOND_SLEEP);
    }
}

void defeat_blinking(){
    unsigned char *mem_base;
    mem_base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);
    loose_rgb_blinking();
    //firs led form left
    unsigned int left_LED_start = 0x80000000,
    //second led form right
                 right_LED_start = 0x00000002;
    for (int i = 0; i < LED_NUMBER/2; i ++){
        if (i % 2 == 0) {
            *(volatile uint32_t *) (mem_base + SPILED_REG_LED_LINE_o) = left_LED_start ;
            left_LED_start /= 4;
        }
        else {
            *(volatile uint32_t *) (mem_base + SPILED_REG_LED_LINE_o) = right_LED_start ;
            right_LED_start *= 4;
        }
        usleep(SLEEP_PERIOD_DEFEAT);
    }
    //firs led form left
    left_LED_start = 0x80000000;
    for (int i = 0; i < LED_NUMBER; i ++){
        *(volatile uint32_t *) (mem_base + SPILED_REG_LED_LINE_o) = left_LED_start;
        left_LED_start /= 2;
        usleep(SLEEP_PERIOD);
    }
    //firs led form right
    unsigned int right_LED_second_anim = 0x00000001;
    for (int i = 0; i <  LED_NUMBER; i ++){
        *(volatile uint32_t *) (mem_base + SPILED_REG_LED_LINE_o) = right_LED_second_anim;
        right_LED_second_anim *= 2;
        usleep(SLEEP_PERIOD);
    }
    // all leds are switched on
    *(volatile uint32_t *) (mem_base + SPILED_REG_LED_LINE_o) = 0xFFFFFFFF;
    usleep(SECOND_SLEEP);
    // all leds are switched off
    *(volatile uint32_t *) (mem_base + SPILED_REG_LED_LINE_o) = 0;
}

void start_game_blinking(){
    unsigned char *mem_base;
    mem_base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);
    start_game_rgb_blinking();
    //firs led form left
    unsigned int LED_from_left = 0x80000000,
    //firs led form right
                 LED_from_right = 0x00000001,
                 temp_LED_left = LED_from_left,
                 temp_LED_right = LED_from_right,
                 sec_temp_LED_left = LED_from_left,
                 sec_temp_LED_right = LED_from_right;
    for (int i = 0; i < LED_NUMBER/2; i ++){
        *(volatile uint32_t *) (mem_base + SPILED_REG_LED_LINE_o) = LED_from_left + LED_from_right;
        temp_LED_left /=2;
        temp_LED_right *= 2;
        LED_from_left += temp_LED_left;
        LED_from_right += temp_LED_right;
        usleep(SECOND_SLEEP / 5);
    }
    // all leds are switched on
    LED_from_left = 0xFFFFFFFF;
    for (int i = 0; i < LED_NUMBER/2; i ++){
        LED_from_left -= (sec_temp_LED_left + sec_temp_LED_right);
        sec_temp_LED_left /=2;
        sec_temp_LED_right *= 2;
        *(volatile uint32_t *) (mem_base + SPILED_REG_LED_LINE_o) = LED_from_left;
        usleep(SECOND_SLEEP / 5);
    }
    for (int i = 0; i < 2; i ++){
        // all leds are switched on
        *(volatile uint32_t *) (mem_base + SPILED_REG_LED_LINE_o) = 0xFFFFFFFF;
        usleep(SECOND_SLEEP);
        // all leds are switched off
        *(volatile uint32_t *) (mem_base + SPILED_REG_LED_LINE_o) = 0;
        usleep(SECOND_SLEEP);
    }
}

void ate_food_blink(){
    unsigned char *mem_base;
    mem_base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);
    eat_food_rgb_blinking();
    //firs led form left
    unsigned int left_LED_start = 0x80000000,
    //firs led form right
            right_LED_start = 0x00000001,
            temp_LED_left = left_LED_start,
            temp_LED_right = right_LED_start;
    for (int i = 0; i < LED_NUMBER/2; i ++){
        *(volatile uint32_t *) (mem_base + SPILED_REG_LED_LINE_o) = left_LED_start + right_LED_start;
        temp_LED_left /=2;
        temp_LED_right *= 2;
        left_LED_start += temp_LED_left;
        right_LED_start += temp_LED_right;
        usleep(EATING_FOOD_PERIOD);
    }
    // all leds are switched off
    *(volatile uint32_t *) (mem_base + SPILED_REG_LED_LINE_o) = 0;
}

int get_blinking_mode(){
    int mode;
    pthread_mutex_lock(&(main_snake.mtx));
    mode = main_snake.blinking_mode;
    pthread_mutex_unlock(&(main_snake.mtx));
    return mode;
}
void set_blink(int mode ){
    pthread_mutex_lock(&(main_snake.mtx));
    main_snake.blinking_mode=mode;
    pthread_mutex_unlock(&(main_snake.mtx));
}

void* blinking_action(void *d){
    info(BLINK_THREAD_STARTED);
    while(!need_quit()){
        int mode = get_blinking_mode();
        if( mode == EAT_FOOD){
            ate_food_blink();
            set_blink(NONE);
        }
        else if( mode == START_GAME){
            start_game_blinking();
            set_blink(NONE);
        }
        else if( mode == WIN_BLINKING){
            win_blinking();
            set_blink(NONE);
        }
        else if( mode == DEFEAT_BLINKING){
            defeat_blinking();
            set_blink(NONE);
        }
    }
    info(BLINK_THREAD_FINISHED);
    return NULL;
}
