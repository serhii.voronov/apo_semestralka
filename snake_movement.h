#ifndef HW_06_SNAKE_MOVEMENT_H
#define HW_06_SNAKE_MOVEMENT_H


#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include <inttypes.h>
#include <string.h>
#include <stdbool.h>
#include <pthread.h>
#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "LED.h"
#include "food_job.h"
#include "keyboard_input.h"
#include "snake_movement_AI.h"

#include "LCD.h"
#include "menu.h"
#include "mutex_get_set.h"

#define MAK_SNAKE_LENGTH 50
#define SLEEP_PERIOD 10000
#define SECOND_SLEEP 250000
#define SLEEP_PERIOD_DEFEAT 50000
#define LED_NUMBER 32

#define WHITE_COLOUR 0xFFFF
#define BLACK_COLOUR 0x0
#define RED_COLOUR 0xF800
#define GREEN_COLOUR 0x07E0

#define DISPLAY_HEIGHT 320
#define DISPLAY_WIDTH 480

#define SNAKE_SIZE_X 20
#define SNAKE_SIZE_Y 20

#define SNAKE_STEP_SIZE 20

#define START_SNAKE_Y 100
#define START_SNAKE_X 100

#define LOW_SPEED 300
#define MIDDLE_SPEED 200
#define HIGH_SPEED 100
/*
 * struct that has all necessary data about snake and the game at all
 */
typedef struct {
    //quit status
    bool quit;
    //pause status
    bool pause;
    bool start_game_status;
    bool need_food;
    //current direction of snake
    int direction;
    int score;
    //current length of snake
    int length;
    //coordinates for food
    int food_x;
    int food_y;
    //array that represents the snake and has all snakes coordinates
    int snake_array[50][2];
    // game mode that was setted before start game
    int mode;
    int blinking_mode;
    //colour of snake that was setted before start game
    int16_t color;
    //game speed that was setted before start game
    int game_speed;
    pthread_mutex_t mtx;
    pthread_cond_t cond;
} SNAKE_T;

extern SNAKE_T main_snake;
/*
 * enum that represents all possible snake directions
 */
enum directions{RIGHT, UP, LEFT, DOWN};
enum food_position{X_FOOD,Y_FOOD};
/*
 * enum that represents current game mode
 */
enum user_ai_mode{USER_MODE,AI_MODE};
/*
 * function helps us to know the current snake length
 * returns current snake length
 */
int get_snake_length();

/*
 * function makes snake longer wen she eats food
 */
void make_snake_bigger();

/*
 * function helps us to know the quit status
 * returns false if not quit
 * returns true if quit
 */
bool need_quit();

/*
 * function for thread to make all actions with snake
 * takes some void pointer
 */
void* snake_moving(void *d);

/*
 * function helps us to know the current direction of the snake
 * returns the current direction of the snake
 */
int get_direction();

/*
 * function draws a square on the screen
 * x_start is the x coord of the left top corner of the square
 * y_start is the y coord of the left top corner of the square
 */
void draw_square(int x_start, int y_start);

/*
 * function controls the collision for snake with herself
 * takes x and y coords of some point in which we want to control the collision
 * returns true if collision happens else false
 */
bool control_collision(int x, int y);
/*
 * simple function for making small sleep.
 * in fact this function define the freekvency and speed of our snake object
 */
void snake_sleep(int speed);


#endif //HW_06_SNAKE_MOVEMENT_H
